#!/bin/bash

#This script is for piping's demonstration. It is at the sending end

#Global vars
COUNT="0"

#Script starts

while true; do
    echo "$COUNT"
    COUNT=$(( "$COUNT" + 1 ))
done
