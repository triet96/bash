#!/bin/bash

#This script is for demonstration for bash_shell_feature


#Global vars
USER_INPUT=""
COUNT="0"
#Script starts
echo proces\'s name is "$0"

while [[ "$COUNT" -lt 5 ]]; do 
    echo Type something and enter
    read USER_INPUT
    echo Your input is: "$USER_INPUT"
    COUNT=$(( $COUNT + 1 ))
done
